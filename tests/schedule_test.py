import logging
from datetime import time, datetime, timedelta

from pytz import timezone
from telegram import User

from chat import Chat
from schedule import scheduler, jobs

logging.getLogger('mensa_thumb_bot').setLevel(logging.CRITICAL)
logging.getLogger('schedule').setLevel(logging.CRITICAL)
logging.getLogger('apscheduler.scheduler').setLevel(logging.CRITICAL)


def mock_send_reminder(chat, time_slot):
    print(chat, time_slot)


def test_schedule():
    scheduler.start()
    chat = Chat('21102015')
    user1 = User(0, 'Steven', False, 'Seagull', 'stevenC', 'en-Us', None)
    user2 = User(1, 'Juice', False, 'Willis', None, 'en-Us', None)
    t = time(14)
    chat.validate_and_process_time_query('14', user1, mock_send_reminder)
    assert t in jobs
    job = jobs[t]
    job_run_time = datetime.combine(datetime.today().date(), t) - timedelta(minutes=10)
    assert job.trigger.run_date == timezone('Europe/Berlin').localize(job_run_time)
    assert job.func == mock_send_reminder
    assert job.args == (chat, t)

    t = time(13)
    chat.validate_and_process_time_query('13', user2, mock_send_reminder)
    assert t in jobs
    job = jobs[t]
    job_run_time = datetime.combine(datetime.today().date(), t) - timedelta(minutes=10)
    assert job.trigger.run_date == timezone('Europe/Berlin').localize(job_run_time)
    assert job.func == mock_send_reminder
    assert job.args == (chat, t)

    chat.validate_and_process_time_query('👎', user2)
    assert t not in jobs
    assert len(jobs) == 1
    assert not job.pending
    scheduler.shutdown(wait=False)
