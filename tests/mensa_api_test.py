from mensa_api import get_tables, Menu, parse_table
from datetime import datetime

test_html = ''

with open('tests/test-mensa.html', 'r', encoding='utf-8') as testfile:
    test_html = testfile.read()


def test_parse_table():
    tables = get_tables(test_html)
    header, body = tables[0]

    expected_menus = [
        Menu('Tagesmenü', 'Paniertes Hähnchenschnitzel mit Spinatcremesoße', '2,70'),
        Menu('Vegetarisch', 'Champignontasche mit Spinatcremesoße', '2,70'),
        Menu('Vital', 'Kalbsgeschnetzeltes mit Mandel-Broccoli und Eierspätzle', '3,70'),
        Menu('Suppe', 'Möhren-Ingwer-Suppe', '0,80'),
        Menu('Action-Theke', 'Burrito mit Chili con Carne gefüllt, dazu Wedges und Chunky-Salsa-Dip auch zum Mitnehmen!', '4,50'),
        Menu('Action-Theke', 'Burrito mit Chili sin Carne gefüllt, dazu Wedges und Chunky-Salsa-Dip auch zum Mitnehmen!', '4,50')
    ]    

    day, menus = parse_table(header, body)

    assert day == datetime(2019, 1, 7).date()
    assert len(menus) == len(expected_menus) + 1

    for i in range(len(menus)-1):
        assert menus[i] == expected_menus[i]

    # Test sides separately because set orders are non-deterministic...

    assert menus[len(menus)-1].name == 'Beilagen'
    assert menus[len(menus)-1].price == None
    
    sides = menus[len(menus)-1].text.split(', ')
    assert set(sides) == set(['Heidelbeer-Dickmilch oder Eis', 'grüne Bohnen', 'Penne Rigate', 'Bananenquark', 'Erbsen-Möhren-Mais-Gemüse', 'Vanillepudding', 'Basmatireis', 'Beilagensalate'])



