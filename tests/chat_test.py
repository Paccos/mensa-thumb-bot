import logging

from telegram import User

from datetime import time

from chat import parse_time, Chat

logging.getLogger('mensa_thumb_bot').setLevel(logging.CRITICAL)
logging.getLogger('schedule').setLevel(logging.CRITICAL)
logging.getLogger('apscheduler.scheduler').setLevel(logging.CRITICAL)


def test_process_time_query():
    chat = Chat('4815162342')
    user = User(0, 'MockyMcMockface', False, '', None, 'en-Us', None)
    # no change without activeSuggestion
    for query in ['👍', '👎']:
        assert not chat.validate_and_process_time_query(query, user)
        assert chat.times() == set()
        assert chat.users_to_time == dict()

    # different query types
    t = time(13)
    assert chat.validate_and_process_time_query('13:00', user)
    assert chat.times() == {t}
    assert chat.users_to_time == {user.id: t}
    for query in ['13', '1', '1?', '1!', '1👍', '👍', ' 👍 ']:
        assert not chat.validate_and_process_time_query(query, user)
        assert chat.times() == {t}
        assert chat.users_to_time == {user.id: t}
    # change time
    t = time(14)
    assert chat.validate_and_process_time_query('14', user)
    assert chat.times() == {t}
    assert chat.users_to_time == {user.id: t}
    # remove user
    assert chat.validate_and_process_time_query('👎', user)
    assert chat.times() == set()
    assert chat.users_to_time == {}
    # no changes if user not in users
    assert not chat.validate_and_process_time_query('👎', user)
    assert chat.times() == set()
    assert chat.users_to_time == {}

    assert not chat.validate_and_process_time_query('Sehr geehrte Damen und Herren,\n'
                                                    'mir würde es sehr zusprechen wenn es möglich wäre das Geschäftsessen auf 13:00 MEZ zu legen\n'
                                                    'Hochachtungsvoll\n'
                                                    'Mocky McMockface', user)
    assert not chat.validate_and_process_time_query('ne, also ich komm nicht mit in die mensa', user)

    assert chat.validate_and_process_time_query('👍', user)
    assert chat.times() == {t}
    assert chat.users_to_time == {user.id: t}
    # no change if both thumbs
    assert not chat.validate_and_process_time_query('👍👎', user)
    assert chat.times() == {t}
    assert chat.users_to_time == {user.id: t}
    assert not chat.validate_and_process_time_query('15', user)
    assert chat.times() == {t}
    assert chat.users_to_time == {user.id: t}


def test_status():
    chat = Chat('528491')
    user1 = User(0, 'Steven', False, 'Seagull', 'stevenC', 'en-Us', None)
    user2 = User(1, 'Juice', False, 'Willis', None, 'en-Us', None)
    user3 = User(2, 'Mark', False, None, None, 'en-Us', None)
    # no time
    assert chat.generate_status() == ("Bislang will wohl niemand essen gehen."
                                      "🤔 Du kannst jedoch einfach eine Zeit vorschlagen!\n"
                                      "*Beispiel:* _1?_")
    # only one time and person
    assert chat.validate_and_process_time_query('14', user1)
    assert chat.generate_status() == ("*Bislang wollen mit:*\n\n"
                                      "*Um 14:00 Uhr:*\n"
                                      "Steven S.\n\n"
                                      "Ich schicke diese Nachricht aber noch kurz vorher rum. 😉"
                                      )
    # one time two persons
    assert chat.validate_and_process_time_query('14!', user2)
    assert chat.generate_status() == ("*Bislang wollen mit:*\n\n"
                                      "*Um 14:00 Uhr:*\n"
                                      "Steven S., Juice W. _(2 Leute)_\n\n"
                                      "Ich schicke diese Nachricht aber noch kurz vorher rum. 😉"
                                      )
    # two times one person
    assert chat.validate_and_process_time_query('13.', user2)
    assert chat.generate_status() == ("*Bislang wollen mit:*\n\n"
                                      "*Um 13:00 Uhr:*\n"
                                      "Juice W.\n"
                                      "*Um 14:00 Uhr:*\n"
                                      "Steven S.\n\n"
                                      "Ich schicke diese Nachricht aber noch kurz vorher rum. 😉"
                                      )

    # Switch time slot to earlier time by user1, then suggest later time by user2 (to see if list stays sorted)
    assert chat.validate_and_process_time_query('1 👍', user1)
    assert chat.validate_and_process_time_query('14.13 ?', user2)

    assert chat.generate_status() == ("*Bislang wollen mit:*\n\n"
                                      "*Um 13:00 Uhr:*\n"
                                      "Steven S.\n"
                                      "*Um 14:15 Uhr:*\n"
                                      "Juice W.\n\n"
                                      "Ich schicke diese Nachricht aber noch kurz vorher rum. 😉"
                                      )

    # two times, two persons in one slot, one without last name
    chat.validate_and_process_time_query('13?', user3)
    assert chat.generate_status() == ("*Bislang wollen mit:*\n\n"
                                      "*Um 13:00 Uhr:*\n"
                                      "Steven S., Mark _(2 Leute)_\n"
                                      "*Um 14:15 Uhr:*\n"
                                      "Juice W.\n\n"
                                      "Ich schicke diese Nachricht aber noch kurz vorher rum. 😉"
                                      )
    chat.validate_and_process_time_query('👎', user1)
    chat.validate_and_process_time_query('👎', user2)
    chat.validate_and_process_time_query('👎', user3)

    assert chat.generate_status() == ("Bislang will wohl niemand essen gehen."
                                      "🤔 Du kannst jedoch einfach eine Zeit vorschlagen!\n"
                                      "*Beispiel:* _1?_")


def test_time_parse():
    assert parse_time('12') == time(12)
    assert parse_time('13') == time(13)
    assert parse_time('14') == time(14)
    assert parse_time('14:00') == time(14)
    assert parse_time('14.00') == time(14)
    assert parse_time('14:01') == time(14)
    assert parse_time('14:08') == time(14, 15)
    assert parse_time('14:16') == time(14, 15)
    assert parse_time('14:30') == time(14, 30)
    assert parse_time('1') == time(13)
    assert parse_time('2') == time(14)
    assert parse_time('halb 12') == time(11, 30)
    assert parse_time('halb 1') == time(12, 30)
    assert parse_time('halb 2') == time(13, 30)
    assert parse_time('halb') is None
    assert parse_time('/menu') is None


def test_reminder():
    chat = Chat('528491')
    user1 = User(0, 'Steven', False, 'Seagull', 'stevenC', 'en-Us', None)

    assert chat.validate_and_process_time_query('14', user1)
    assert chat.reminder(chat.users_to_time[user1.id]) == ("*Bislang wollen mit:*\n\n"
                                                           "*Um 14:00 Uhr:*\n"
                                                           "Steven S.\n\n"
                                                           "*14:00 Uhr-Gruppe, Zeit zum Aufbrechen! 🚶‍♀️🚶*"
                                                           )
