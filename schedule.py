import logging

from pytz import timezone

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.jobstores.base import JobLookupError

from datetime import datetime, timedelta, time, date

logger = logging.getLogger(__name__)

jobs = dict()
scheduler = BackgroundScheduler(timezone=timezone('Europe/Berlin'))


def add_reminder(time_slot, func, chat):
    job_run_time = datetime.combine(date.today(), time_slot) - timedelta(minutes=10)

    job = scheduler.add_job(func, 'date', run_date=job_run_time, args=[chat, time_slot])
    jobs[time_slot] = job
    logger.info("New time slot! Scheduled status message for " + str(job_run_time))


def remove_job(time_slot):
    if time_slot not in jobs:
        return
    logger.info("No people anymore for {}. Removing status message job.".format(time_slot))
    job = jobs[time_slot]

    try:
        job.remove()
        logger.info("Removed status message job for " + str(time_slot))
    except JobLookupError:
        logger.info("Job has already been removed for " + str(time_slot))
    del jobs[time_slot]  # remove from dictionary


def schedule_cleaning(func, chat):
    job_run_time = datetime.combine(date.today(), time(14, 25))

    scheduler.add_job(func, 'date', run_date=job_run_time, args=[chat])
