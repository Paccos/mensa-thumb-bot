.PHONY: lint test pipeline

pipeline:
	pycodestyle *.py --ignore=E501 && python3 -m pytest tests && echo 'pipeline ran successfully'

lint:
	pycodestyle *.py --ignore=E501

test:
	python3 -m pytest tests -v